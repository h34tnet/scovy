package net.h34t.scovy

import com.squareup.kotlinpoet.ClassName
import net.h34t.scovy.util.ucFirst

/**
 * Maps database types to types that are actually used.
 *
 * This is because
 * a) we want native kotlin types instead of java types and
 * b) some jdbc-reported types seem off, like `[B` for ByteArray
 *
 * Additionally, resultSet accessors (getX, setX) have to be defined explicitly for typecasting or
 * matching
 */
open class TypeMapper {

    private fun type(sqlType: String, kotlinType: String, accessor: String, needsTypeCast: Boolean = false) =
        Type(
            sqlType,
            ClassName.bestGuess(kotlinType),
            "get${accessor.ucFirst()}",
            "set${accessor.ucFirst()}",
            needsTypeCast = needsTypeCast
        )

    private fun type(sqlType: String, accessor: String, needsTypeCast: Boolean = false) =
        Type(
            sqlType,
            ClassName.bestGuess(sqlType),
            "get${accessor.ucFirst()}",
            "set${accessor.ucFirst()}",
            needsTypeCast = needsTypeCast
        )

    private val basicTypes = listOf(
        type("java.lang.String", "kotlin.String", "String"),
        type("java.lang.Character", "kotlin.Char", "Char"),

        type("java.lang.Boolean", "kotlin.Boolean", "Boolean"),

        type("java.lang.Integer", "kotlin.Int", "Int"),
        type("java.lang.Byte", "kotlin.Byte", "Byte"),
        type("java.lang.Short", "kotlin.Short", "Short"),
        type("java.lang.Long", "kotlin.Long", "Long"),

        type("java.lang.Float", "kotlin.Float", "Float"),
        type("java.lang.Double", "kotlin.Double", "Double"),

        type("java.lang.Number", "kotlin.Number", "Number"),
        // type("java.math.BigDecimal", "kotlin.BigDecimal", "BigDecimal"),

        type("java.lang.Object", "kotlin.Any", "Object"),
        type("java.sql.Blob", "java.io.InputStream", "BinaryStream"),
        type("java.sql.Clob", "java.io.Reader", "CharacterStream"),
        type("java.sql.InputStream", "java.io.InputStream", "BinaryStream"),

        type("java.sql.Time", "java.sql.Time", "Time"),
        type("java.sql.Timestamp", "java.sql.Timestamp", "Timestamp"),
        type("java.sql.Date", "java.sql.Date", "Date"),
        type("[B", "kotlin.ByteArray", "Bytes"),
        type("java.sql.SQLXML", "SQLXML"),
        type("java.net.URL", "URL")

    ).associateBy { it.sqlType }

    fun getMapping(type: Int, typeName: String, className: String, isNullable: Boolean): Type = try {
        (basicTypes[className] ?: type(className, className, "Object", needsTypeCast = true))
            .let {
                // if necessary, make the type nullable
                if (!isNullable) it else it.copy(isNullable = true)
            }
    } catch (e: ClassNotFoundException) {
        throw Exception("No class found for SQL-type: \"$typeName\" ($type): \"$className\"", e)
    }
}

data class Type(
    val sqlType: String,
    val typeClass: ClassName,
    val getter: String,
    val setter: String,
    val isNullable: Boolean = false,
    val needsTypeCast: Boolean = false
)
