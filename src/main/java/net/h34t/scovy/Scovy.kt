package net.h34t.scovy

import com.squareup.kotlinpoet.FileSpec
import net.h34t.scovy.entity.QueryType
import net.h34t.scovy.generator.DMLGenerator
import net.h34t.scovy.generator.DQLGenerator
import net.h34t.scovy.query.parseMetadata
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.sql.Connection

/**
 * Creates DAO (and in case of selects DTO) classes via KotlinPoet.
 */
class Scovy(
    private val logger: Logger = LoggerFactory.getLogger("Scovy"),
    private val typeMapper: TypeMapper = TypeMapper(),
    private val largeUpdates: Boolean = false
) {

    /**
     * @return the filespec for the class
     */
    fun process(con: Connection, packageName: String, basename: String, query: String): FileSpec {
        // parses metadata (i.e. nullability overrides)
        val metaData = parseMetadata(query)

        return when (val queryType = QueryType.determine(query)) {
            QueryType.SELECT -> {
                DQLGenerator().create(
                    logger,
                    typeMapper,
                    con,
                    packageName,
                    basename,
                    query,
                    metaData
                )
            }

            QueryType.INSERT, QueryType.UPDATE, QueryType.DELETE -> {
                DMLGenerator().create(
                    logger,
                    typeMapper,
                    con,
                    queryType,
                    packageName,
                    basename,
                    query,
                    largeUpdates,
                    metaData
                )
            }
        }
    }
}