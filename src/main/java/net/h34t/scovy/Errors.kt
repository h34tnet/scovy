package net.h34t.scovy

/**
 * Thrown if the heuristics for determining the query type for an SQL query fail.
 *
 * If this is the case, try to remove the SQL file in question by adding it to the file filter.
 */
class UnsupportedQueryType(message: String) : Exception(message)
