package net.h34t.scovy.util

import java.io.File

/**
 * Converts the first char to uppercase, leaves the rest as-is.
 */
fun String.ucFirst() = this.replaceFirstChar { it.uppercaseChar() }

/**
 * Returns a list of all files that match the filter in the given directory tree.
 */
fun gatherInputFiles(dir: File, filter: ((File) -> Boolean)? = null): List<File> =
    dir.walk()
        .filter { it.isFile && filter?.invoke(it) ?: true }
        .toList()

/**
 * Creates a package name (i.e. dot-separated) from a (relative) file path.
 */
fun File.toPackageName(): String =
    if (parentFile != null) {
        "${parentFile.toPackageName()}.$name"
    } else {
        this.name
    }
