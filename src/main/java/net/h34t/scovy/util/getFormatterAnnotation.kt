package net.h34t.scovy.util

import com.squareup.kotlinpoet.AnnotationSpec

/**
 * Adds file annotations to suppress kotlin style analyzer complaints (which are pointless for generated code).
 */
fun getFormatterAnnotation() =
    AnnotationSpec.builder(Suppress::class)
        .useSiteTarget(AnnotationSpec.UseSiteTarget.FILE)
        .addMember("%S", "RedundantVisibilityModifier")
        .addMember("%S", "UnusedImport")
        .addMember("%S", "LocalVariableName")
        .addMember("%S", "PropertyName")
        .addMember("%S", "unused")
        .addMember("%S", "SpellCheckingInspection")
        .build()