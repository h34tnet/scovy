package net.h34t.scovy.util

/**
 * Splits a string by a regex separator and returns both the segments and the separators.
 */
@OptIn(ExperimentalStdlibApi::class)
fun String.splitExp(regex: Regex): List<SplitResult> {
    val results = mutableListOf<SplitResult>()
    var pos = 0

    do {
        val mr = regex.find(this, pos)

        if (mr != null) {
            results.add(SplitResult.Segment(this.substring(pos..<mr.range.first)))
            results.add(SplitResult.Separator(mr))
            pos = mr.range.last + 1
        } else {
            results.add(SplitResult.Segment(this.substring(pos)))
        }
    } while (mr != null)

    return results
}

sealed class SplitResult {
    data class Separator(val match: MatchResult) : SplitResult() {
        override fun toString() = match.value
    }

    data class Segment(val value: String) : SplitResult() {
        override fun toString() = value
    }
}
