package net.h34t.scovy.query

import net.h34t.scovy.entity.QueryVariant
import net.h34t.scovy.util.SplitResult
import net.h34t.scovy.util.splitExp

/**
 * Splits the query into its named variants.
 */
fun splitIntoVariants(query: String) =
    query.splitExp(variantPattern).let { parts ->
        if (parts.size == 1)
            listOf(QueryVariant("", (parts.first() as SplitResult.Segment).value))
        else {
            val head = (parts.first() as SplitResult.Segment).value.trim()
            val tail = parts.drop(1).windowed(size = 2, step = 2)

            tail.map { res ->
                QueryVariant(
                    name = (res.first() as SplitResult.Separator).match.groupValues[1],
                    query = head + "\n" + (res[1] as SplitResult.Segment).value.trim()
                )
            }
        }
    }

private val variantPattern = Regex("\\s*---:\\s*([a-zA-Z][a-zA-Z0-9]*)\\s*", RegexOption.MULTILINE)
