package net.h34t.scovy.query

import net.h34t.scovy.entity.PositionalParameterQuery

/**
 * Turns a query with named placeholders into a query with positional parameters and returns it with the position to
 * param name mapping.
 */
fun extractNamedParameters(query: String): PositionalParameterQuery {
    // Replace the named parameters with unnamed parameters and store an index-to-name mapping.
    val parameters = mutableListOf<String>()
    val positionalQuery = query.replace(paramPattern) {
        it.value.substring(1).let { paramName ->
            verifyParamName(paramName)
            parameters.add(paramName)
            "?"
        }
    }

    return PositionalParameterQuery(
        positionalQuery,
        parameters.toList()
    )
}

private val paramPattern = Regex(":[a-zA-Z][a-zA-Z0-9_-]*")

private fun verifyParamName(name: String) {
    require(name.first() == name.first().lowercaseChar()) {
        "Param $name must start with a lowercase letter."
    }
}
