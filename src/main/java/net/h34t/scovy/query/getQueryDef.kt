package net.h34t.scovy.query

import net.h34t.scovy.TypeMapper
import net.h34t.scovy.entity.ParameterDef
import net.h34t.scovy.entity.QueryDef
import org.slf4j.Logger
import java.sql.Connection
import java.sql.ParameterMetaData
import java.sql.ResultSetMetaData

/**
 * Queries the parameter and result column metadata necessary to generate the DTOs and DAOs.
 */
fun getQueryDef(
    log: Logger,
    typeMapper: TypeMapper,
    con: Connection,
    name: String,
    query: String,
    paramNames: List<String>,
    metadata: List<MetaData>
) =
    con.prepareStatement(query).use { stmt ->
        val pmd = stmt.parameterMetaData

        val params = (1..pmd.parameterCount).map { i ->
            ParameterDef(
                idx = i,
                name = paramNames[i - 1],
                type = typeMapper.getMapping(
                    type = pmd.getParameterType(i),
                    typeName = pmd.getParameterTypeName(i),
                    className = pmd.getParameterClassName(i),
                    isNullable = isMetaNull(paramNames[i - 1], metadata) ?: when (val x = pmd.isNullable(i)) {
                        ParameterMetaData.parameterNullable -> true
                        ParameterMetaData.parameterNoNulls -> false
                        ParameterMetaData.parameterNullableUnknown -> {
                            log.warn(
                                "Nullability for ${paramNames[i - 1]} " +
                                        "(${pmd.getParameterTypeName(i)}) is unknown."
                            )
                            true
                        }

                        else -> throw IllegalStateException("Illegal return value for isNullable: $x")
                    }
                )
            )
        }

        val cmd = stmt.metaData

        val columns = if (cmd != null) {
            (1..cmd.columnCount).map { i ->
                ParameterDef(
                    idx = i,
                    name = cmd.getColumnName(i),
                    type = typeMapper.getMapping(
                        type = cmd.getColumnType(i),
                        typeName = cmd.getColumnTypeName(i),
                        className = cmd.getColumnClassName(i),
                        isNullable = isMetaNull(cmd.getColumnName(i), metadata) ?: when (val x =
                            cmd.isNullable(i)) {
                            ResultSetMetaData.columnNullable -> true
                            ResultSetMetaData.columnNoNulls -> false
                            ResultSetMetaData.columnNullableUnknown -> {
                                log.warn(
                                    "Nullability for ${cmd.getColumnName(i)} " +
                                            "(${cmd.getColumnTypeName(i)}) is unknown."
                                )
                                true
                            }

                            else -> throw IllegalStateException("Illegal return value for isNullable: $x")
                        }
                    ),
                )
            }
        } else {
            emptyList()
        }

        QueryDef(name, query, params, columns)
    }
