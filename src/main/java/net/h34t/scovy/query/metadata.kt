package net.h34t.scovy.query


/**
 * All metadata definitions.
 */
sealed class MetaData {
    /**
     * Defines all columns as nullable.
     */
    data object AllNull : MetaData()

    /**
     * Defines all columns as non-nullable.
     */
    data object NoneNull : MetaData()

    /**
     * Defines the given columns as nullable.
     */
    class Null(val cols: List<String>) : MetaData()

    /**
     * Defines the given columns as non-nullable.
     */
    class NotNull(val cols: List<String>) : MetaData()
}

/**
 * Extracts metadata markup from the SQL string.
 *
 * Metadata markup has the form `-- @name` or `--@name: value` and must be defined as a subclass of MetaData, otherwise
 * it'll be ignored.
 */
fun parseMetadata(query: String): List<MetaData> =
    query.lines()
        .mapNotNull {
            if (it.trim().startsWith("--")) {
                if (metaKey.matches(it)) {
                    when (metaKey.find(it)?.groupValues?.get(1)) {
                        "AllNull" -> MetaData.AllNull
                        "NoneNull" -> MetaData.NoneNull
                        else -> null
                    }

                } else if (metaKeyValue.matches(it)) {
                    val match = metaKeyValue.find(it)

                    when (match?.groupValues?.get(1)) {
                        "Null" -> MetaData.Null(parseColumnNames(match.groupValues[2]))
                        "NotNull", "NonNull" -> MetaData.NotNull(parseColumnNames(match.groupValues[2]))
                        else -> null
                    }
                } else {
                    null
                }
            } else null
        }

private val metaKey = Regex("\\s*--\\s*@(\\w+)\\s*")
private val metaKeyValue = Regex("\\s*--\\s*@(\\w+)\\s*:\\s*([\\w,\\s]*)")

private val listSep = Regex("\\s*,\\s*")
private val columnNameValidator = Regex("\\w+")

/**
 * Parse the MetaData's "value" string into a list of column names.
 */
private fun parseColumnNames(commaSeparated: String): List<String> =
    commaSeparated
        .split(listSep)
        .map { it.lowercase() }
        .onEach {
            require(columnNameValidator.matches(it))
        }

/**
 * Find the last metadata nullability rule that matches the given column and evaluate it.
 *
 * @return true if nullable, false if non-nullable or null if no rule matches.
 */
fun isMetaNull(columnName: String, metaData: List<MetaData>): Boolean? = columnName.lowercase().let { name ->
    metaData.lastOrNull {
        (it is MetaData.AllNull ||
                it is MetaData.NoneNull ||
                (it is MetaData.Null && it.cols.contains(name)) ||
                (it is MetaData.NotNull && it.cols.contains(name)))
    }?.let {
        // and see whether it evaluates to null or non-null
        it is MetaData.AllNull || it is MetaData.Null
    }
}