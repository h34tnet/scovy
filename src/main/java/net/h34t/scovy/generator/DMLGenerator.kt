package net.h34t.scovy.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import net.h34t.scovy.TypeMapper
import net.h34t.scovy.entity.QueryType
import net.h34t.scovy.entity.createResultSetSetter
import net.h34t.scovy.query.MetaData
import net.h34t.scovy.query.extractNamedParameters
import net.h34t.scovy.query.getQueryDef
import net.h34t.scovy.query.splitIntoVariants
import net.h34t.scovy.util.getFormatterAnnotation
import net.h34t.scovy.util.ucFirst
import org.slf4j.Logger
import java.io.Closeable
import java.sql.Connection
import java.sql.PreparedStatement

/**
 * Generator for DML (insert, update, delete) queries.
 */
class DMLGenerator {

    /**
     * Creates the file spec for a DML query, i.e. the DTO (if applicable) and all DAO methods.
     */
    fun create(
        logger: Logger,
        typeMapper: TypeMapper,
        con: Connection,
        type: QueryType,
        packageName: String,
        basename: String,
        query: String,
        largeUpdates: Boolean = false,
        metaData: List<MetaData>
    ): FileSpec {
        val funPrefix = when (type) {
            QueryType.INSERT -> "insert"
            QueryType.UPDATE -> "update"
            QueryType.DELETE -> "delete"
            else -> throw Exception("This generator is for DML only and can't be used with type $type.")
        }

        val className = basename.replaceFirstChar { it.uppercase() }

        val queryRes = splitIntoVariants(query)

        val queryDefs = queryRes.map { qr ->
            extractNamedParameters(qr.query).let { dq ->
                getQueryDef(logger, typeMapper, con, qr.name, dq.query, dq.paramName, metaData)
            }
        }

        val file = FileSpec.builder(packageName, basename)

        queryDefs.forEach { queryDef ->
            file.addType(
                TypeSpec.classBuilder(ClassName(packageName, "${className}${queryDef.name.ucFirst()}DAO"))
                    .addSuperinterface(Closeable::class)
                    .primaryConstructor(
                        FunSpec.constructorBuilder().addParameter("con", Connection::class)
                            .build()
                    )
                    .addProperty(
                        PropertySpec.builder("stmt", PreparedStatement::class, KModifier.PRIVATE)
                            .initializer(CodeBlock.builder().add("con.prepareStatement(%S)", queryDef.query).build())
                            .build()
                    )
                    .addFunction(
                        FunSpec.builder(funPrefix)
                            .addParameters(queryDef.params.map { param ->
                                ParameterSpec.builder(param.name, param.type.typeClass).build()
                            })
                            .returns(Int::class)
                            .addCode(
                                CodeBlock.builder()
                                    .apply {
                                        queryDef.params.forEach { col ->
                                            this.add(col.createResultSetSetter("this.stmt"))
                                        }
                                    }
                                    .add("return this.stmt.executeUpdate()\n")
                                    .build()
                            )
                            .build()
                    )
                    .addFunction(
                        FunSpec.builder("batch${funPrefix.ucFirst()}")
                            .addParameters(queryDef.params.map { param ->
                                ParameterSpec.builder(param.name, param.type.typeClass).build()
                            })
                            .addCode(
                                CodeBlock.builder()
                                    .apply {
                                        queryDef.params.forEach { col ->
                                            this.add(col.createResultSetSetter("this.stmt"))
                                        }
                                    }
                                    .add("this.stmt.addBatch()\n")
                                    .build()
                            )
                            .build()
                    )
                    .addFunction(
                        FunSpec.builder("executeBatch")
                            .returns(IntArray::class)
                            .addCode("return this.stmt.executeBatch()")
                            .build()
                    )
                    .apply {
                        if (largeUpdates) {
                            addFunction(
                                FunSpec.builder("large${funPrefix.ucFirst()}")
                                    .addParameters(queryDef.params.map { param ->
                                        ParameterSpec.builder(param.name, param.type.typeClass).build()
                                    })
                                    .returns(Long::class)
                                    .addCode(
                                        CodeBlock.builder()
                                            .apply {
                                                queryDef.params.forEach { col ->
                                                    this.add(col.createResultSetSetter("this.stmt"))
                                                }
                                            }
                                            .add("return this.stmt.executeLargeUpdate()\n")
                                            .build()
                                    )
                                    .build()
                            )

                            addFunction(
                                FunSpec.builder("executeLargeBatch")
                                    .returns(LongArray::class)
                                    .addCode("return this.stmt.executeLargeBatch()")
                                    .build()
                            )
                        }
                    }
                    .addFunction(
                        FunSpec.builder("close")
                            .addModifiers(KModifier.OVERRIDE)
                            .addCode("this.stmt.close()")
                            .build()
                    )
                    .build()
            )
        }

        return file
            .addAnnotation(getFormatterAnnotation())
            .addFileComment("@formatter:off")
            .build()
    }
}