package net.h34t.scovy.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import net.h34t.scovy.entity.ParameterDef

/**
 * Creates a DTO for the given parameter definitions.
 *
 * Note: this method should never actually throw the IllegalStateException, it's just for enforcing nullability.
 */
fun createDTO(className: ClassName, columnInfo: List<ParameterDef>): TypeSpec {
    val types = columnInfo.associate {
        val nullable = it.type.isNullable

        it.name to it.type.typeClass.copy(nullable = nullable)
    }

    return TypeSpec.classBuilder(className)
        .addModifiers(KModifier.DATA)
        .primaryConstructor(
            FunSpec
                .constructorBuilder()
                .apply {
                    columnInfo.forEach {
                        this.addParameter(
                            it.name, types[it.name]
                                ?: throw IllegalStateException("Unknown type name ${it.name}")
                        )
                    }
                }
                .build())
        .addProperties(
            columnInfo.map {
                PropertySpec
                    .builder(
                        it.name, types[it.name]
                            ?: throw IllegalStateException("Unknown type name ${it.name}")
                    )
                    .initializer(it.name)
                    .build()
            }
        )
        .build()
}
