package net.h34t.scovy.generator

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.LambdaTypeName
import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import com.squareup.kotlinpoet.TypeVariableName
import com.squareup.kotlinpoet.asClassName
import net.h34t.scovy.TypeMapper
import net.h34t.scovy.entity.ParameterDef
import net.h34t.scovy.entity.QueryDef
import net.h34t.scovy.entity.createResultSetGetter
import net.h34t.scovy.entity.createResultSetSetter
import net.h34t.scovy.query.MetaData
import net.h34t.scovy.query.extractNamedParameters
import net.h34t.scovy.query.getQueryDef
import net.h34t.scovy.query.splitIntoVariants
import net.h34t.scovy.util.getFormatterAnnotation
import net.h34t.scovy.util.ucFirst
import org.slf4j.Logger
import java.io.Closeable
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet

/**
 * Generator for `select` queries.
 */
class DQLGenerator() {

    /**
     * Creates the file spec for the whole source query, i.e. a DTO and DAOs for all query variants.
     */
    fun create(
        log: Logger,
        typeMapper: TypeMapper,
        con: Connection,
        packageName: String,
        basename: String,
        query: String,
        metadata: List<MetaData>
    ): FileSpec {
        val className = basename.replaceFirstChar { it.uppercase() }
        val dtoClassName = ClassName(packageName, "${className}DTO")

        // Split the input query into variants which are separated by `---: variantName`.
        // Variants must happen below the column selection as all variants share the same DTO.
        val variants = splitIntoVariants(query)

        val qData = variants.map { qr ->
            extractNamedParameters(qr.query).let { dq ->
                getQueryDef(log, typeMapper, con, qr.name, dq.query, dq.paramName, metadata)
            }
        }

        // Check whether all DTOs are the same for all variants
        require(qData.map { it.columns }.toSet().size == 1) { "Columns differ between query variants." }

        val sections = qData.map { it to it.params }

        val dto = createDTO(dtoClassName, qData.first().columns)

        return FileSpec
            .builder(packageName, basename)

            // For IntelliJ: prevents autoformatting of generated files,
            // which would lead to changes in git even on identical files.
            .addAnnotation(getFormatterAnnotation())
            .addFileComment("@formatter:off")
            .addType(dto)
            .apply {
                create(this, packageName, basename, dtoClassName, sections)
            }
            .addFunction(createResultSetExtractor(dtoClassName))
            .addFunction(createResultSetToDTO(dtoClassName, qData.first().columns))
            .build()
    }

    /**
     * Creates a DAO for each query variant.
     */
    private fun create(
        fileSpecBuilder: FileSpec.Builder,
        packageName: String,
        baseName: String,
        dtoClassName: ClassName,
        queryDef: List<Pair<QueryDef, List<ParameterDef>>>
    ) {
        queryDef.forEach { qd ->
            fileSpecBuilder.addType(createQuery(ClassName(packageName, baseName.ucFirst() + qd.first.name.ucFirst() + "DAO"), dtoClassName, qd.first.query, qd.second))
        }
    }

    /**
     * Creates a DAO for a query variant.
     */
    private fun createQuery(
        daoClassName: ClassName,
        dtoName: ClassName,
        query: String,
        paramDef: List<ParameterDef>
    ): TypeSpec {
        val names = paramDef.groupBy { it.name }

        val returnType = List::class.asClassName().parameterizedBy(dtoName)

        val transformerType = TypeVariableName("T")

        val mapBlockTypeName = LambdaTypeName.get(
            parameters = arrayOf(dtoName),
            returnType = transformerType
        )

        return TypeSpec.classBuilder(daoClassName)
            .addSuperinterface(Closeable::class)
            .primaryConstructor(
                FunSpec.constructorBuilder()
                    .addParameter("con", Connection::class)
                    .addCode(
                        CodeBlock.builder()
                            .add("this.stmt = con.prepareStatement(%S)", query)
                            .build()
                    )
                    .build()
            )
            .addProperty(PropertySpec.builder("stmt", PreparedStatement::class).addModifiers(KModifier.PRIVATE).build())
            .addFunction(
                FunSpec.builder("fetch")
                    .addParameters(names.map { ParameterSpec(it.key, it.value.first().type.typeClass) })
                    .addCode(CodeBlock.builder()
                        .indent()
                        .apply {
                            paramDef.forEach { col ->
                                this.add(col.createResultSetSetter("this.stmt"))
                            }
                        }
                        .add("return this.stmt.executeQuery().use { _rs -> extractResults(_rs) }")
                        .unindent()
                        .build())
                    .returns(returnType)
                    .build()
            )
            .addFunction(
                FunSpec.builder("map")
                    .addTypeVariable(transformerType)
                    .addParameters(names.map { ParameterSpec(it.key, it.value.first().type.typeClass) })
                    .addParameter(ParameterSpec.builder("_transformer", mapBlockTypeName).build())
                    .addCode(CodeBlock.builder()
                        .indent()
                        .apply {
                            paramDef.forEach { col ->
                                this.add(col.createResultSetSetter("this.stmt"))
                            }
                        }
                        .add("this.stmt.executeQuery().use { _rs ->\n")
                        .indent()
                        .add("""
                            |val _result = mutableListOf<%T>()
                            |while (_rs.next())
                            |  _result.add(_transformer.invoke(resultSetToDTO(_rs)))
                            |
                            |return _result
                        """.trimMargin(), transformerType)
                        .unindent()
                        .add("\n}")
                        .unindent()
                        .build())
                    .returns(List::class.asClassName().parameterizedBy(transformerType))
                    .build()
            )
            .addFunction(
                FunSpec.builder("close")
                    .addModifiers(KModifier.OVERRIDE)
                    .addCode(
                        CodeBlock.builder()
                            .add("this.stmt.close()")
                            .build()
                    )
                    .build()
            )
            .build()
    }

    /**
     * Creates a method that converts the [ResultSet] into a list of DTOs.
     */
    private fun createResultSetExtractor(dtoName: ClassName): FunSpec {
        val returnType = List::class.asClassName().parameterizedBy(dtoName)

        val body = CodeBlock.builder()
            .indent()
            .add(
                """
                |val dtos = mutableListOf<%T>()
                |while (resultSet.next())
                |  dtos.add(resultSetToDTO(resultSet))
                |
                |return dtos
            """.trimMargin(),
                dtoName
            )
            .unindent()
            .build()

        return FunSpec.builder("extractResults")
            .addParameter("resultSet", ResultSet::class).addModifiers(KModifier.PRIVATE)
            .addCode(body)
            .returns(returnType)
            .build()
    }

    /**
     * Creates a function that converts a [ResultSet] into a DTO.
     */
    private fun createResultSetToDTO(dtoName: ClassName, columnDef: List<ParameterDef>): FunSpec {
        val body = CodeBlock.builder()
            .add("return %T(\n", dtoName)
            .indent()
            .apply {
                columnDef.forEach { col ->
                    this.add(col.createResultSetGetter("resultSet"))
                    this.add(",\n")
                }
            }
            .add(")")
            .unindent()
            .build()

        return FunSpec.builder("resultSetToDTO")
            .addModifiers(KModifier.PRIVATE)
            .addParameter("resultSet", ResultSet::class)
            .addCode(body)
            .returns(dtoName)
            .build()
    }
}