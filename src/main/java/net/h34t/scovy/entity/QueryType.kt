package net.h34t.scovy.entity

import net.h34t.scovy.UnsupportedQueryType

/**
 * The query type - DQL (select) or DML (insert, update, delete).
 *
 * Used for code generation and naming.
 */
enum class QueryType {
    SELECT, INSERT, UPDATE, DELETE;

    /**
     * Attempts to determine the QueryType of a query by looking at the first word of the first non-empty, non-comment
     * line of the query.
     */
    companion object {
        private val multilineComment = Regex("/\\*[\\S\\s]*?\\*/", RegexOption.MULTILINE)
        private val whitespace = Regex("\\s")

        fun determine(query: String): QueryType =
            query
                // remove multiline comments
                .replace(multilineComment, "")
                .lines()
                // remove empty lines
                .filter { it.isNotBlank() }
                // remove single line comments
                .first { !it.trim().startsWith("--") }
                .trim()
                .split(whitespace, 2).first().lowercase().let { type ->
                    when (type) {
                        "select" -> SELECT
                        "insert" -> INSERT
                        "update" -> UPDATE
                        "delete" -> DELETE
                        else -> throw UnsupportedQueryType(
                            "Unsupported query type: $type. Please use only line comments '-- ...'."
                        )
                    }
                }
    }
}