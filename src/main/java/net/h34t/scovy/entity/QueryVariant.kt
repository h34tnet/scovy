package net.h34t.scovy.entity

/**
 * Contains a query variant.
 */
data class QueryVariant(
    /**
     * The name of the variant (or empty if the source query contained no variants)
     */
    val name: String,

    /**
     * The resulting query.
     */
    val query: String
)