package net.h34t.scovy.entity

import com.squareup.kotlinpoet.CodeBlock
import net.h34t.scovy.Type

/**
 * Describes a named parameter or the column of a result.
 */
data class ParameterDef(
    /**
     * The position in the ResultSet.
     */
    val idx: Int,
    /**
     * The name of the column or parameter.
     */
    val name: String,

    /**
     * The data type and its access methods.
     */
    val type: Type,
)

fun ParameterDef.createResultSetGetter(resultSetName: String): CodeBlock =
    if (type.needsTypeCast)
        CodeBlock.builder().add("%N = %N.getObject(%L, %T::class.java)", name, resultSetName, idx, type.typeClass)
            .build()
    else
        CodeBlock.builder().add("%N = %N.%N(%L)", name, resultSetName, type.getter, idx).build()

fun ParameterDef.createResultSetSetter(stmt: String): CodeBlock =
    if (type.needsTypeCast)
        CodeBlock.builder().add("%L.setObject(%L, %N)\n", stmt, idx, name).build()
    else
        CodeBlock.builder().add("%L.%N(%L, %N)\n", stmt, type.setter, idx, name).build()
