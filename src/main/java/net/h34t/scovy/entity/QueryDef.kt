package net.h34t.scovy.entity

/**
 * The name, translated source and parameter/column metadata of a query.
 */
data class QueryDef(
    /**
     * The variant name or blank if it's the only variant.
     */
    val name: String,
    /**
     * The SQL query.
     */
    val query: String,

    /**
     * The list of query parameters.
     */
    val params: List<ParameterDef>,

    /**
     * The list of result columns.
     */
    val columns: List<ParameterDef>
)