package net.h34t.scovy.entity

/**
 * A positional parameter query that has been created from a named parameter query and its parameter-name to position
 * mapping.
 */
data class PositionalParameterQuery(
    /**
     * The query with positional parameters.
     */
    val query: String,

    /**
     * The names of the parameters at their 0-based positions.
     */
    val paramName: List<String>
)