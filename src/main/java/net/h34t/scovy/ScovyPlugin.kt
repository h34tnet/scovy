package net.h34t.scovy

import net.h34t.scovy.util.gatherInputFiles
import net.h34t.scovy.util.toPackageName
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.Logging
import java.io.File
import java.io.IOException
import java.sql.DriverManager
import kotlin.io.path.createDirectories
import kotlin.io.path.isWritable

/**
 * The Scovy gradle plugin class.
 */
@Suppress("unused")
class ScovyPlugin : Plugin<Project> {
    private val logger = Logging.getLogger(ScovyPlugin::class.java)

    override fun apply(project: Project) {
        val extension = project.extensions.create("scovy", ScovyExtension::class.java, project)

        // add sourceSets for every configuration with addSources = true
        project.afterEvaluate {
            val sourceSets =
                project.extensions.getByName("sourceSets") as org.gradle.api.tasks.SourceSetContainer

            extension.configurations.forEach { c ->
                if (c.addSources) {
                    logger.lifecycle("Adding Scovy sourceSet ${c.outputDirectory}")

                    sourceSets.getByName("main").apply {
                        java.srcDir(c.outputDirectory)
                    }
                } else {
                    logger.debug("Not adding scovy sourceSet for ${c.outputDirectory}")
                }
            }
        }

        project.task("scovy") { task ->
            task.doLast {
                extension.configurations.forEachIndexed { configIdx, config ->
                    config.apply {
                        try {
                            val scovy = Scovy(logger)
                            logger.lifecycle("Processing config ${configIdx + 1}: $sqlFileDirectory")

                            val inputDir = File(sqlFileDirectory)
                            val outputDir = File(outputDirectory)

                            val dsn = dsn

                            val user = user
                            val password = password

                            if (!inputDir.exists() || !inputDir.isDirectory || !inputDir.canRead())
                                throw IOException("SQL file directory \"$inputDir\" doesn't exist, is not a directory or is not readable.")

                            if (outputDir.exists() && !outputDir.isDirectory) {
                                throw IOException("Output path \"$outputDir\" isn't a directory.")
                            } else if (!outputDir.exists()) {
                                outputDir.toPath().let { path ->
                                    try {
                                        path.createDirectories()
                                    } catch (e: Exception) {
                                        throw IOException("Couldn't create output directories for path \"$path\".", e)
                                    }
                                }
                            }

                            if (!outputDir.toPath().isWritable()) {
                                throw IOException("Output directory \"$outputDir\" is not writeable.")
                            }

                            // Load the given driver if available
                            driver.takeIf { it.isNotEmpty() }?.let {
                                try {
                                    Class.forName(it)
                                } catch (e: ClassNotFoundException) {
                                    throw ClassNotFoundException(
                                        "Couldn't find driver \"$it\". Make sure the driver is present at build by adding " +
                                                "`buildscript { dependencies { classpath(\"...\") } }` to your " +
                                                "build.gradle.kt.",
                                        e
                                    )
                                }
                            }

                            DriverManager.getConnection(dsn, user, password).use { con ->
                                val files = gatherInputFiles(inputDir, fileFilter)

                                files.forEachIndexed { fileIdx, file ->
                                    val relativePath = file.relativeTo(inputDir)

                                    val basename = file.nameWithoutExtension
                                    val packageName = file.parentFile.relativeTo(inputDir).toPackageName()
                                    val query = file.readText()

                                    try {
                                        scovy.process(
                                            con,
                                            packageName = packageName,
                                            basename = basename,
                                            query = query
                                        )
                                            .writeTo(outputDir)

                                        logger.lifecycle(" - ${fileIdx + 1}: Successfully processed $relativePath.")
                                    } catch (e: Exception) {
                                        logger.lifecycle(" - ${fileIdx + 1}: Processing $relativePath failed: ${e.message}.")
                                        throw e
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            throw e
                        }
                    }
                }
            }
        }
    }
}

open class ScovyExtension(@Suppress("unused") val project: Project) {
    val configurations = mutableListOf<ScovyConfig>()

    @Suppress("unused")
    fun add(configuration: ScovyConfig.() -> Unit) {
        configurations.add(ScovyConfig().apply(configuration))
    }

    inner class ScovyConfig {
        var sqlFileDirectory: String = ""
        var outputDirectory: String = "${project.buildDir}/generated-sources/scovy"
        var addSources: Boolean = true
        var driver: String = ""
        var dsn: String = ""
        var user: String = ""
        var password: String = ""
        var fileFilter: (File) -> Boolean = { true }
    }
}
