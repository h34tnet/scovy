package net.h34t.scovy.generator

import net.h34t.scovy.query.MetaData
import net.h34t.scovy.query.isMetaNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class IsMetaNullTest {

    @Test
    fun `test is null`() {
        assertNull(isMetaNull("foo", emptyList()))
    }

    @Test
    fun `test is meta all null`() {
        assertTrue(isMetaNull("foo", listOf(MetaData.AllNull)) ?: false)
    }

    @Test
    fun `test is meta all not null`() {
        val isNull = isMetaNull("foo", listOf(MetaData.NoneNull))
        requireNotNull(isNull)

        assert(isNull == false)
    }

    @Test
    fun `test is meta col null`() {
        assertTrue(isMetaNull("foo", listOf(MetaData.Null(listOf("foo", "bar")))) ?: false)
    }

    @Test
    fun `test is meta col not null`() {
        val isNull = isMetaNull("bar", listOf(MetaData.NotNull(listOf("foo", "bar"))))
        requireNotNull(isNull)
        assert(isNull == false)
    }
}
