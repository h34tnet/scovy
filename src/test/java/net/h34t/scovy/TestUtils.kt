package net.h34t.scovy

import java.sql.Connection

fun Connection.test(block: (con : Connection) -> Unit) {
    try {
        block(this)
    } finally {
        this.rollback()
    }
}


