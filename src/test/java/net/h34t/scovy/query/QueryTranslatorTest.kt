package net.h34t.scovy.query

import org.junit.jupiter.api.Test

class QueryTranslatorTest {


    @Test
    fun `test query translator empty`() {
        val (query, placeholders) = extractNamedParameters("SELECT foo FROM bar")

        assert(query == "SELECT foo FROM bar")
        assert(placeholders.isEmpty())
    }

    @Test
    fun `test query translator one`() {
        val (query, placeholders) = extractNamedParameters("SELECT foo FROM bar WHERE x = :boo")

        assert(query == "SELECT foo FROM bar WHERE x = ?")
        assert(placeholders.size == 1)
        assert(placeholders.first() == "boo")
    }

    @Test
    fun `test query translator two`() {
        val (query, placeholders) = extractNamedParameters("SELECT foo FROM bar WHERE x = :boo AND y < :bar")

        assert(query == "SELECT foo FROM bar WHERE x = ? AND y < ?")
        assert(placeholders.size == 2)
        assert(placeholders[0] == "boo")
        assert(placeholders[1] == "bar")
    }

    @Test
    fun `test query translator three`() {
        val (query, placeholders) = extractNamedParameters("SELECT foo FROM bar WHERE x > :boo AND x < :boo")

        assert(query == "SELECT foo FROM bar WHERE x > ? AND x < ?")
        assert(placeholders.size == 2)
        assert(placeholders[0] == "boo")
        assert(placeholders[1] == "boo")
    }

}