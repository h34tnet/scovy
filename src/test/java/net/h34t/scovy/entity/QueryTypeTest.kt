package net.h34t.scovy.entity

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class QueryTypeTest {

    @Test
    fun `test regular`() {
        assertEquals(QueryType.SELECT, QueryType.determine("SELECT * FROM bar"))
        assertEquals(QueryType.INSERT, QueryType.determine("insert into bar (name) values (:name)"))
        assertEquals(QueryType.UPDATE, QueryType.determine("updaTE bar SET name = :newName"))
        assertEquals(QueryType.DELETE, QueryType.determine("Delete   FROM bar WHERe ID=:id"))
    }

    @Test
    fun `test singleline comments`() {
        assertEquals(
            QueryType.SELECT, QueryType.determine(
                """
            |-- hello world
            |-- this is a generated file
            |
            |SELECT * FROM bar
        """.trimMargin()
            )
        )
    }

    @Test
    fun `test multiline comments`() {
        assertEquals(
            QueryType.SELECT,
            QueryType.determine("""/* hello world this is a generated file */ SELECT * FROM bar""")
        )

        assertEquals(
            QueryType.SELECT, QueryType.determine(
                """
            |/*
            |  hello world
            |  this is a generated file 
            |*/
            |
            |SELECT * FROM bar
        """.trimMargin()
            )
        )

        assertEquals(
            QueryType.SELECT, QueryType.determine(
                """
            |/*
            |  hello world
            |  this is a generated file 
            |*/ SELECT * FROM bar
        """.trimMargin()
            )
        )
    }

    @Test
    fun `test unrecognized`() {
        assertThrows<Exception> {
            QueryType.determine("TRUNCATE foo")
        }
    }
}