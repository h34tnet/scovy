package net.h34t.scovy.util

import org.junit.jupiter.api.Test

class RegexSplitTest {


    @Test
    fun `test splitter`() {
        val text = "foo 123 bar 456 boo"

        val res = text.splitExp(Regex("\\d+"))

        assert(res.size == 5)
        assert(res[0] is SplitResult.Segment)
        assert(res[1] is SplitResult.Separator)
        assert(res[2] is SplitResult.Segment)
        assert(res[3] is SplitResult.Separator)
        assert(res[4] is SplitResult.Segment)
        assert(res[0].toString() == "foo ")
        assert(res[1].toString() == "123")
        assert(res[2].toString() == " bar ")
        assert(res[3].toString() == "456")
        assert(res[4].toString() == " boo")
    }

    @Test
    fun `test nomatch`() {
        val text = "foo bar baz"
        val res = text.splitExp(Regex("\\d+"))

        assert(res.size == 1)
        assert(res[0] is SplitResult.Segment)
        assert(res[0].toString() == "foo bar baz")
    }

    @Test
    fun `test empty`() {
        val text = ""
        val res = text.splitExp(Regex("\\d+"))

        assert(res.size == 1)
        assert(res[0] is SplitResult.Segment)
        assert(res[0].toString() == "")
    }

    @Test
    fun `test splitter only`() {
        val text = "123"
        val res = text.splitExp(Regex("\\d+"))

        assert(res.size == 3)
        assert(res[0] is SplitResult.Segment)
        assert(res[0].toString() == "")
        assert(res[1] is SplitResult.Separator)
        assert(res[1].toString() == "123")
        assert(res[2] is SplitResult.Segment)
        assert(res[2].toString() == "")
    }

    @Test
    fun `test strsplit`() {
        assert("|||".split('|').size == 4)
    }
}