package net.h34t.scovy

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.sql.Connection
import java.sql.DriverManager

object ScovySmokeTest {

    private lateinit var con: Connection

    @BeforeEach
    fun before() {
        con = DriverManager.getConnection("jdbc:h2:mem:testdb")
        con.autoCommit = false

        con.createStatement()
            .execute("CREATE TABLE test(id IDENTITY NOT NULL PRIMARY KEY, name VARCHAR(255), confirmed boolean not null default false, birth_date TIMESTAMP WITHOUT TIME ZONE)")

        con.commit()
    }

    @AfterEach
    fun after() {
        con.createStatement().execute("DROP TABLE test")
        con.commit()
    }

    @Test
    fun `test for smoke`() {
        this.con.test { con ->
            val scovy = Scovy()
            val select = scovy.process(con, "a.b.c", "test", "SELECT * FROM test")
            val insert = scovy.process(con, "a.b.c", "insertTest", "insert into test (id, name, confirmed, birth_date) values (:id, :name, false, :birth-date)")
            val update = scovy.process(con, "a.b.c", "updateTest", "update test SET confirmed = :confirmed WHERE id = :id")
            val delete = scovy.process(con, "a.b.c", "deleteTest", "DELETE FROM test WHERE confirmed = false")

            assert(select.toString().isNotEmpty())
            assert(insert.toString().isNotEmpty())
            assert(update.toString().isNotEmpty())
            assert(delete.toString().isNotEmpty())
        }
    }

    @Test
    fun `test variants for smoke`() {
        this.con.test { con ->
            val scovy = Scovy()
            val select = scovy.process(con, "a.b.c", "test", "SELECT * FROM test ---: byId WHERE id = :id ---: all ---: unconfirmed WHERE confirmed = false")

            val source = select.toString()
            println(source)

            assert(source.isNotEmpty())
            assert(source.contains("class TestByIdDAO("))
            assert(source.contains("class TestUnconfirmedDAO("))
            assert(source.contains("class TestAllDAO("))
        }
    }

    @Test
    fun `test data types`() {
        con.test { con ->
            con.createStatement()
                .execute(
                    """
                |CREATE TABLE datatypes (
                |  col_character CHARACTER,
                |  col_charvar CHARACTER VARYING,
                |  col_char_lob CHARACTER LARGE OBJECT,
                |  col_varcharignorecase VARCHAR_IGNORECASE,
                |  col_binary BINARY,
                |  col_binary_varying BINARY VARYING,
                |  col_bin_lob BINARY LARGE OBJECT,
                |  col_boolean BOOLEAN,
                |  col_tinyint TINYINT,
                |  col_smallint SMALLINT,
                |  col_integer INTEGER,
                |  col_bigint BIGINT,
                |  col_numeric NUMERIC,
                |  col_real REAL,
                |  col_double DOUBLE PRECISION,
                |  col_decfloat DECFLOAT,
                |  col_date DATE,
                |  col_time TIME,
                |  col_time_tz TIME WITH TIME ZONE,
                |  col_timestamp TIMESTAMP,
                |  col_timestamp_tz TIMESTAMP WITH TIME ZONE,
                |  col_interval INTERVAL MINUTE,
                |  col_javaobj JAVA_OBJECT,
                |  col_enum ENUM('foo', 'bar', 'baz'),
                |  col_geometry GEOMETRY,
                |  col_json JSON,
                |  col_uuid UUID,
                |  col_intarray INTEGER ARRAY,
                |  col_boolarray BOOLEAN ARRAY
                |)
            """.trimMargin()
                )
            con.commit()

            val scovy = Scovy()

            val sourceSelect = scovy.process(con, "a.b.c", "datatypesSelect", "select * FROM datatypes").toString()
            val sourceInsert = scovy.process(con, "a.b.c", "datatypesInsert", "INSERT INTO datatypes (col_bin_lob) values (:col_bin_lob)").toString()

            println(sourceSelect)
            println(sourceInsert)

            assert(sourceSelect.isNotEmpty())
            assert(sourceInsert.isNotEmpty())
        }
    }

    @Test
    fun `test map`() {
        con.test {
            val select = Scovy().process(con, "a.b.c", "test", "SELECT * FROM test WHERE id = :id")

            val source = select.toString()

            assert(source.isNotEmpty())
            println(source)
        }
    }

    @Test
    fun `test nullability meta`() {
        val sourceBase = Scovy().process(
            con, "a.b.c", "test", "" +
                    """
                    SELECT ID, NAME FROM test
                """
        ).toString()
        assert(sourceBase.contains("public val ID: Long,"))
        assert(sourceBase.contains("public val NAME: String?,"))

        val sourceAllNull = Scovy().process(
            con, "a.b.c", "test", "" +
                    """
                    -- @AllNull
                    SELECT ID, NAME FROM test
                """
        ).toString()
        assert(sourceAllNull.contains("public val ID: Long?,"))
        assert(sourceAllNull.contains("public val NAME: String?,"))

        val sourceNoneNull = Scovy().process(
            con, "a.b.c", "test", "" +
                    """
                    -- @NoneNull
                    SELECT ID, NAME FROM test
                """
        ).toString()

        assert(sourceNoneNull.contains("public val ID: Long,"))
        assert(sourceNoneNull.contains("public val NAME: String,"))

        println(sourceAllNull)
        println(sourceNoneNull)
    }

    @Test
    fun `test column nullability meta both`() {
        val sourceColsNull = Scovy().process(
            con, "a.b.c", "test", "" +
                    """
                    -- @Null: ID
                    -- @NotNull: NAME
                    SELECT * FROM test
                """
        ).toString()
        println(sourceColsNull)

        assert(sourceColsNull.contains("public val ID: Long?,"))
        assert(sourceColsNull.contains("public val NAME: String,"))
    }

    @Test
    fun `test column nullability meta`() {
        val sourceColsNull = Scovy().process(
            con, "a.b.c", "test", "" +
                    """
                    -- @Null: ID, NAME
                    SELECT * FROM test
                """
        ).toString()
        println(sourceColsNull)

        assert(sourceColsNull.contains("public val ID: Long?,"))
        assert(sourceColsNull.contains("public val NAME: String?,"))
    }

    @Test
    fun `test column nullability meta all+not`() {
        val source = Scovy().process(
            con, "a.b.c", "test", "" +
                    """
                    -- @AllNull
                    -- @NotNull: NAME, BIRTH_DATE
                    SELECT * FROM test
                """
        ).toString()
        println(source)

        assert(source.contains("public val ID: Long?,"))
        assert(source.contains("public val NAME: String,"))
    }
}