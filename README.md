# Scovy

Scovy is a gradle plugin that generates JDBC DAO and DTO kotlin source from SQL query files using a metadata from
prepared statements, allowing for type-safe and null-aware access and the same ResultSet-to-DTO conversion speed as
handcrafted code.

## Why use Scovy?

* Enables IDE autocompletion and type-checking
* Performance - as there's no runtime penalty for type conversion.

## Configuration

In your `build.gradle.kts`, add the following task configuration (given a PostgreSQL database):

```kotlin
buildscript {
    // ...
    dependencies {
        // add the database driver to the build dependencies
        classpath("org.postgresql:postgresql:42.6.0")
    }
}

plugins {
    id("net.h34t.scovy") version "0.1.0"
}

// ...

scovy {
    add {
        sqlFileDirectory = "$projectDir/sql"
        outputDirectory = "$projectDir/src/main/kotlin"
        dsn = "jdbc:postgresql://localhost:5432/dbname"
        driver = "org.postgresql.Driver"
        user = "dbuser"
        password = "dbpassword"
        fileFilter = { file ->
            file.extension == "sql"
        }
    }
}
```

This collects all files that pass the `fileFilter` from the `sqlFileDirectory` and writes the resulting files to the
`outputDirectory`.

It's possible to define multiple configuration blocks.

## Example

Given the table `account`:

```sql
CREATE TABLE IF NOT EXISTS demo.account
(
    id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    uuid uuid,
    email character varying(256) COLLATE pg_catalog."default" NOT NULL,
    displayname character varying(256) COLLATE pg_catalog."default",
    password bytea,
    confirmed boolean NOT NULL,
    created timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT user_pkey PRIMARY KEY (id)
)
```

The following SQL file `account.sql`:  
(Note that the separator `---: name` allows for multiple ways to access the same DTO)

```
SELECT * FROM demo.account
---: byID
WHERE id = :id
---: byName
WHERE displayname LIKE :name AND confirmed = :isConfirmed
```

generates DTO and DAO classes:

```kotlin
public data class AccountDTO(
    public val id: Int,
    public val uuid: java.util.UUID?,
    public val email: String,
    public val displayname: String?,
    public val password: ByteArray?,
    public val confirmed: Boolean,
    public val created: java.sql.Timestamp,
)

class AccountByIdDAO(con: java.sql.Connection) : Closeable {
    fun fetch(id: Int): List<AccountDTO> {
        ...
    }
}

class AccountByNameDAO(con: java.sql.Connection) : Closeable {
    public fun fetch(name: String, isConfirmed: Boolean): List<AccountDTO> {
        ...
    }
}
```

```kotlin
fun main() {
    java.sql.DriverManager.getConnection("jdbc:...").use { con ->
        AccountByNameDAO(con).use { dao ->
            dao.fetch("John%").forEach { println(it.email) }
        }
    }
}
```

Note that all variants must return the same columns. To query different columns, create a new SQL file.

DML queries, i.e. `INSERT`, `UPDATE` and `DELETE`, are also supported:

The file `accountInsert.sql`:

```sql
INSERT INTO demo.account (uuid, email, displayname, password, confirmed) VALUES (:uuid, :email, :displayname, :password, false)
```

creates the following DAO classes:

```kotlin
public class AccountInsertDAO(con: java.sql.Connection) : Closeable {
    // Omits creation of an intermediate DTO instance  
    public fun insert(uuid: java.util.UUID, email: String, displayname: String, password: ByteArray, ): Int {
        ...
    }

    // Bulk inserts
    public fun bulkInsert(uuid: java.util.UUID, email: String, displayname: String, password: ByteArray, ) {
        ...
    }

    public fun executeBulk(): IntArray {
        ...
    }
}
```

DMLs support variants and don't create DTO classes.

## Nullability

Due to the inability of many database drivers to correctly infer result column nullability (e.g. during joins) it's
possible to manually override nullability using metadata in SQL comments.

Currently, four different metadata commands are supported:

```sql
-- @AllNull
-- @NoneNull (or @NotNull)
-- @Null: desc
-- @NotNull: id, name
SELECT
  p.ID id, 
  p.name name,
  q.description desc 
FROM
  mytable p
    LEFT OUTER JOIN othertable q ON (p.id = q.parentId)
```

* `@AllNull`: Forces nullability in _all_ result columns
* `@NoneNull`: Forces non-nullability in _all_ result columns
* `@Null: col1, col2, col3`:  Forces nullability in the given result columns
* `@NotNull: col1, col2, col3`:  Forces non-nullability in the given result columns
* `@NonNull: ...` alias for `@NotNull: ...`

Column names and metadata columns are matched case-insensitively because database engines handle column name case
differently.

Rules are applied one after the other, so in the following example all columns except col2 are nullable.

```sql
-- @NoneNull
-- @Null: col2
```

If no rule applies to a column the inferred nullability is kept.

## Notes

* Query parameters are must start with a lowercase letter and contain only letters, digits, underscores and hyphens.
* Parameters are not nullable, only columns in `select` queries.
* SQL input files must be UTF-8 encoded.

## Limitations

* After database schema changes, the application must be recompiled.
* Behaviour may differ between database drivers.
* No generated keys: As implementations differ between drivers and databases and the necessary metadata doesn't get
  populated without actually calling an update, retrieving generated keys (i.e. automatically generating DTOs for keys)
  is not possible. Future updates may support generated keys through manual hints.

## Open issues

* There's no `equals` and `hashCode` implementation for generated DTOs containing `Array`s.
* The provided `TypeMapper` is incomplete.
* Blobs and Streams aren't necessarily correctly handled.

## Supported database systems

Smoke-tested with [PostgreSQL](https://www.postgresql.org/) and [H2](https://www.h2database.com).

Currently, it doesn't work with [SQLite](https://www.sqlite.org) and the 
[xerial driver](https://github.com/xerial/sqlite-jdbc), as there seem to be problems with SQLites dynamic typing. 

Generated Scovy code works with the [HikariCP](https://github.com/brettwooldridge/HikariCP) connection pool. 

## License

MIT License

Copyright (c) 2023 Stefan Schallerl

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
