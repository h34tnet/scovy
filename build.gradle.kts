plugins {
    kotlin("jvm") version "1.9.21"
    `java-gradle-plugin`
    id("com.gradle.plugin-publish") version "1.2.1"

}

group = "net.h34t"
version = "0.1.0"

repositories {
    mavenCentral()
}

dependencies {
    implementation(gradleApi())

    implementation("com.squareup:kotlinpoet:1.15.2")

    testImplementation("com.h2database:h2:2.2.222")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(11)
}

gradlePlugin {
    plugins {
        create("scovy") {
            id = "net.h34t.scovy"
            displayName = "Scovy DAO and DTO Kotlin Code Generator"
            description = "A Gradle plugin to generate type-safe kotlin DAO and DTO code from SQL queries."
            implementationClass = "net.h34t.scovy.ScovyPlugin"
            website.set("https://codeberg.org/h34tnet/scovy")
            vcsUrl.set("https://codeberg.com/h34tnet/scovy")
            tags.set(listOf("kotlin", "code", "sql", "jdbc", "dao", "dto", "generator"))
        }
    }
}
